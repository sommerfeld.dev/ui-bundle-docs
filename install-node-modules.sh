#!/bin/bash

echo -e "$LOG_INFO Install gulp-cli globally"
npm install -g gulp-cli
gulp --version

(
  cd src/main/ui-bundle || exit

  echo -e "$LOG_INFO Clean node modules"
  rm -rf node_modules

  echo -e "$LOG_INFO Install the project's dependencies inside the project folder"
  npm install
)

echo -e "$LOG_DONE Installed all modules to build ui bundle"
