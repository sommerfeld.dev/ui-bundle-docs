= UI Template

The UI Template on which this UI bundle is based is the link:https://themes.getbootstrap.com/product/falcon-admin-dashboard-webapp-template["Falcon – Admin Dashboard & WebApp Template" from Bootstrap Themes]. The used (bought) license is the link:https://themes.getbootstrap.com/licenses/#fullStandardLicense[The Full Standard License] which allows usage for one single product for non-paying users only.

== Template History
. The first template of choice was the link:https://themeforest.net/item/dasho-bootstrap-admin-template/29854435[Dasho Bootstrap Admin Template] using the Regular License (Use in a single product which end users are not charged for).
. Alternatively the free link:https://themes.3rdwavemedia.com/bootstrap-templates/product/coderdocs-free-bootstrap-5-documentation-template-for-software-projects[CoderDocs template] was considered as well.
