#!/bin/bash
# @file ui-bundle-build.sh
# @brief Build the ui bundle locally.
#
# @description The script runs all linters and builds the ui bundle zip-file locally.
#
# ==== Arguments
#
# The script does not accept any parameters.

echo -e "$LOG_INFO Cleanup target directory"
mkdir -p target
rm -rf target/*

(
  cd src/main/ui-bundle || exit

  echo -e "$LOG_INFO Build ui-bundle"
  gulp bundle
)

echo -e "$LOG_INFO Finished local ui-bundle build"
